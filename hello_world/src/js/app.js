import React from 'react';
import ReactDOM from 'react-dom';

const jsx = <h1>Hello World!</h1>
ReactDOM.render(jsx, document.getElementById('root'));
