import React from 'react';
import ReactDOM from 'react-dom';
import Welcome from './Welcome';

/*
function Welcome(props) {
  return <h1>Hello World and peace {props.firstname} {props.lastname}!</h1>
}
*/


ReactDOM.render(<Welcome firstname="Marco" lastname="von Nerdisch"/>, document.getElementById('root'));