import React from 'react';
import ReactDOM from 'react-dom';

const root = document.getElementById('root');

function updateWithoutReact() {
  const html = [
    '<h1>Hello World!</h1>',
    `<h2>It is ${new Date().toLocaleTimeString()}.</h2>`
  ]

  root.innerHTML = html.join('');
}

function updateWithReact() {
  const jsx = (
    <div>
      <h1>Hello World!</h1>
      <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>
  );
  
  ReactDOM.render(jsx, root);
}

window.setInterval(updateWithReact, 1000);

