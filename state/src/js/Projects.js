import React from 'react';

export default class Projects extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true
    }

    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 3000)
  }

  render() {
    return <h1>{ this.state.isLoading ? 'Loading...' : 'Fertig!' }</h1>
  }
}