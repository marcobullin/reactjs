module.exports = {
  entry: './src/js/app.js',
  output: {
    path: `${__dirname}/build/public/`,
    publicPath: 'public/',
    filename: 'bundle.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: 'babel-loader'
    }]
  }
}