import React from 'react';

export default class Loading extends React.Component {
  componentWillMount() {
    console.log('WILL MOUNT');
  }

  componentDidMount() {
    console.log('DID MOUNT');
    // INITIALISIERUNG oder SETUP
  }

  render() {
    console.log('RENDER');
    return <p>Loading ...</p>
  }

  componentWillUnmount() {
    console.log('UNMOUNT');
    // CLEAN UP
  }
}