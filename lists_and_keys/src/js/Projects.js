import React from 'react';
import Loading from './Loading';
import Project from './Project';

export default class Projects extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      projects: ['Reat', 'Webpack', 'Vue']
    }
  }

  loadProjects(foo, bar, e) {
    console.log(foo, bar, e);
    e.preventDefault();

    this.setState({ projects: ['xxx', 'Reat', 'Webpack', 'Vue'] });
  }

  render() {
    const foo = 'foo';
    const bar = 'bar'

    const ProjectComponents = this.state.projects.map(project => <Project  key={project} project={project}/>);
    const ProjectComponents2 = this.state.projects.map(project => <Project  key={project} project={project}/>);

    return (
      <div>
        <ul>{ ProjectComponents }</ul>
        <ul>{ ProjectComponents2 }</ul>
        <a href="#" onClick={ this.loadProjects.bind(this, foo, bar) }>Lade Projekte</a>
      </div>
    );
  }
}