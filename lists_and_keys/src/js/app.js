import React from 'react';
import ReactDOM from 'react-dom';
import Welcome from './Welcome';
import Projects from './Projects';

/*
function Welcome(props) {
  return <h1>Hello World and peace {props.firstname} {props.lastname}!</h1>
}
*/


ReactDOM.render(
  <div>
    <Welcome firstname="Marco" lastname="von Nerdisch"/>
    <Projects/>
  </div>, document.getElementById('root'));