import React from 'react';
import Loading from './Loading';

export default class Projects extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      projects: []
    }
  }

  loadProjects(foo, bar, e) {
    console.log(foo, bar, e);
    e.preventDefault();

    this.setState({ projects: ['Reat', 'Webpack', 'Vue'] });
  }

  render() {
    const foo = 'foo';
    const bar = 'bar'

    const ProjectComponents = this.state.projects.map(project => <li>{ project }</li>);

    return (
      <div>
        <ul>{ ProjectComponents }</ul>
        <a href="#" onClick={ this.loadProjects.bind(this, foo, bar) }>Lade Projekte</a>
      </div>
    );
  }
}